package com.etna.inboxservice;

import com.etna.inboxservice.domain.Message;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InboxServiceApplicationTests {
    static Logger log = Logger.getLogger(InboxServiceApplicationTests.class);

	private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;


    @Autowired
    protected Environment env;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void add_Message_ShouldFindMessage() throws Exception {
        log.info("Running tests"); //should use AOP instead of this
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .build();

        String matchId = UUID.randomUUID().toString();
        String content = generator.generate(100);
        Message message = Message.builder()
                .fromUser("From")
                .toUser("To")
                .content(content)
                .matchId(matchId)
                .build();

        mockMvc.perform(
                post("/devops-inbox/1.0.0/inbox/{matchId}", matchId)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(message))
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.content", is(content)))
                .andExpect(jsonPath("$.match_id", is(matchId)))
                .andExpect(jsonPath("$.from", is("From")))
                .andExpect(jsonPath("$.to", is("To")));

        mockMvc.perform(get("/devops-inbox/1.0.0/inbox/{matchId}", matchId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].content", is(content)))
                .andExpect(jsonPath("$[0].match_id", is(matchId)))
                .andExpect(jsonPath("$[0].from", is("From")))
                .andExpect(jsonPath("$[0].to", is("To")));

    }

}

package com.etna.inboxservice.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chris on 25/08/2017.
 */
@Data
public class InboxItem {
    @JsonProperty("match_id")
    private final String matchId;
}

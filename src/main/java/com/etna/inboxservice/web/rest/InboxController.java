package com.etna.inboxservice.web.rest;

import com.etna.inboxservice.domain.Message;
import com.etna.inboxservice.repository.MessageRepository;
import com.etna.inboxservice.util.SkipOffsetPageable;
import com.etna.inboxservice.web.rest.dto.InboxItem;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by chris on 26/08/2017.
 */
@Controller
@RequestMapping("/devops-inbox/1.0.0")
public class InboxController {

    @Autowired
    private MessageRepository messageRepository;

    @RequestMapping(value="/inbox", method = RequestMethod.GET)
    @ResponseBody
    public List<InboxItem> getInbox(@RequestParam(required = false) Integer skip, @RequestParam(required = false) Integer limit){
        Page<InboxItem> p = messageRepository.findAllInboxItem(new SkipOffsetPageable(skip, limit));
        return p != null ? p.getContent() : null;
    }

    @RequestMapping(value={"/inbox", "/inbox/{matchId}"}, method = RequestMethod.POST)
    public ResponseEntity<Message> addMessage(@PathVariable(required = false) String matchId, @RequestBody Message message){
        if(message.getId() != null && messageRepository.findOne(message.getId()) != null)
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        if (matchId != null)
            message.setMatchId(matchId);
        if (message.getMatchId() == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Message>(messageRepository.save(message), HttpStatus.CREATED);
    }

    @RequestMapping(value= "/inbox/{matchId}", method = RequestMethod.GET)
    public ResponseEntity<?> getConversation(@PathVariable String matchId){
        return new ResponseEntity<>(messageRepository.findAllByMatchId(matchId), HttpStatus.OK);
    }
}

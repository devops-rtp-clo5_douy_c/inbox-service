package com.etna.inboxservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * Created by chris on 25/08/2017.
 */
@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @NotEmpty
    @JsonProperty("from")
    private String fromUser;

    @NotEmpty
    @JsonProperty("to")
    private String toUser;

    @JsonProperty("match_id")
    private String matchId;

    @JsonProperty("sent_date")
    private LocalDateTime sentDate = LocalDateTime.now();

    @JsonProperty("created_date")
    private LocalDateTime createdDate = LocalDateTime.now();

    @NotEmpty
    @Size(min = 1, max = 255)
    private String content;

}

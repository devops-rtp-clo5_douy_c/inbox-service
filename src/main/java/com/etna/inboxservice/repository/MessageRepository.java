package com.etna.inboxservice.repository;


import com.etna.inboxservice.domain.Message;
import com.etna.inboxservice.web.rest.dto.InboxItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by chris on 26/08/2017.
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, String> {

    @Query("select distinct new com.etna.inboxservice.web.rest.dto.InboxItem(i.matchId) from Message i")
    Page<InboxItem> findAllInboxItem(Pageable p);

    List<Message> findAllByMatchId(String matchId);

}
